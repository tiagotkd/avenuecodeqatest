# Welcome 

This project automate the following scenarios:

- Login
- Create task
- Create subtask
- Delete task
- Delete subtask
- Welcome message

## Project structure

```
Project
└───helper: folder with utilities
│   └───pages: Page Objects mapping (.py)
│   └───resources: Step definitions with implementations (.robot)
│   └───specs: Specification of scenarios (.robot)
└───target: Reports and screenshots
│   requirements.txt - Dependencies
```



## Install

Pre requisites:

- Python 3 
- Chromedriver

2. Clone this repo:
	```sh
	$ git clone https://tiagotkd@bitbucket.org/tiagotkd/avenuecodeqatest.git
	```
3. Go to project folder

4. Create a virtualenv
	```sh
	$ virtualenv venv --python=`which python3`
	```
5. Install dependencies:
    ```bash
    $ source venv/bin/activate
    $ pip install -r requirements.txt
    ```
	


## Run
In the project folder execute:
```sh
$ robot -i welcome_message -d target Feature

$ robot -i create_task_with_less_than_tree_chars -d target Feature

$ robot -i create_task_with_more_than_250_chars -d target Feature

$ robot -i create_subtask -d target Feature

$ robot -i delete_task -d target Feature

$ robot -i delete_subtask -d target Feature
```
