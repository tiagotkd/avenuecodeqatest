*** Settings ***
Library                                 SeleniumLibrary
Library                                 ../pages/LoginPage.py
Library                                 ../pages/HomePage.py
Library                                 ../pages/TasksPage.py


*** Keywords ***

that I have been created a task "${task_name}"
    Create Task  ${task_name}

I try to create a new subtask "${subtask_name}" and due date "${due_date}"
    Create Subtask  ${subtask_name}  ${due_date}

the subtask "${subtask_name}" is added in my todo list
    Is Task In List  ${subtask_name}