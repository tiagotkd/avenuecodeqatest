*** Settings ***
Library                                 SeleniumLibrary
Library                                 DebugLibrary
Library                                 ../pages/LoginPage.py
Library                                 ../pages/HomePage.py
Library                                 ../pages/TasksPage.py


*** Keywords ***
that I have been created a task with name "${task_name}"
    Create Task  ${task_name}

I have been created a subtask "${subtask_name}" and due date "${due_date}"
    Create Subtask  ${subtask_name}  ${due_date}

I try to delete this subtask
#    Debug
    Remove Subtask

the subtask "${subtask_name}" is deleted
    ${present}=  run keyword and return status  Is task In List   ${title}
    Run Keyword If   '${present}' == 'False'  Pass Execution  Subtask was deleted