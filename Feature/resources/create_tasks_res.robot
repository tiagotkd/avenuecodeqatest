*** Settings ***
Library                                 SeleniumLibrary
Library                                 BuiltIn
Library                                 ../pages/LoginPage.py
Library                                 ../pages/HomePage.py
Library                                 ../pages/TasksPage.py


*** Keywords ***
that I want create a task
    Log to console  ${\n}create a task

I create a task with title "${title}"
    set global variable  ${title}
    My Tasks
    Create Task  ${title}

the task is not created in the application
    ${present}=  run keyword and return status  Is task In List   ${title}
    Run Keyword If   '${present}' == 'True'  Fail
