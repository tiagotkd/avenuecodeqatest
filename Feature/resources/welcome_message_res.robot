*** Settings ***
Library                                 SeleniumLibrary
Library                                 ../pages/LoginPage.py
Library                                 ../pages/HomePage.py
Library                                 ../pages/TasksPage.py


*** Keywords ***

that I have been signed up
    Open Browser        https://qa-test.avenuecode.com      Chrome
    Maximize Browser Window

I sign in using my e-mail and password
    Go to Login Page
    Login  tiago.goes2009@gmail.com  \#q1w2e3#
    My Tasks

I see a message "${message}"
    Check Welcome Message  ${message}  

