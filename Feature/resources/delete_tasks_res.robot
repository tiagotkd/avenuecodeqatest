*** Settings ***
Library                                 SeleniumLibrary
Library                                 ../pages/LoginPage.py
Library                                 ../pages/HomePage.py
Library                                 ../pages/TasksPage.py


*** Keywords ***
that I have been created a task "${task_name}"
    Create Task  ${task_name}

I try to delete this task
    Remove Task

the task "${task_name}" is deleted
    ${present}=  run keyword and return status  Is task In List   ${title}
    Run Keyword If   '${present}' == 'False'  Pass Execution  Task was deleted