*** Settings ***
Library                                 SeleniumLibrary
Library                                 BuiltIn
Library                                 ../pages/LoginPage.py
Library                                 ../pages/HomePage.py
Library                                 ../pages/TasksPage.py


*** Keywords ***

Prepare Environment
    Open Browser        https://qa-test.avenuecode.com      Chrome
    Maximize Browser Window
    Go to Login Page
    Login  tiago.goes2009@gmail.com  \#q1w2e3#
    My Tasks