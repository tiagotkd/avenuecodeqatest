*** Settings ***
Resource  ../resources/delete_tasks_res.robot
Resource  ../resources/delete_subtask_res.robot
Resource  ../helper/setup.robot

Test Setup  Prepare Environment

*** Test Case ***
Delete a task
    [Tags]  delete_task
    Given that I have been created a task "Create Layout"
    When I try to delete this task
    Then the task "Create Layout" is deleted


Delete a subtask
    [Tags]  delete_subtask
    Given that I have been created a task with name "Create Layout"
    And I have been created a subtask "Choose tool" and due date "25/06/2019"
    When I try to delete this subtask
    Then the subtask "Choose tool" is deleted