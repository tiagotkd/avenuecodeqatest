*** Settings ***
Resource  ../resources/create_tasks_res.robot
Resource  ../resources/create_subtask_res.robot
Resource  ../helper/setup.robot

Test Setup  Prepare Environment

*** Test Case ***
Create a task with less than three characters
    [Tags]  create_task_with_less_than_tree_chars
    Given that I want create a task
    When I create a task with title "AB"
    Then the task is not created in the application


Create a task with less than three characters
    [Tags]  create_task_with_more_than_250_chars
    Given that I want create a task
    When I create a task with title "Example Example Example Example Example Example Example Example Example Example Example Example Example Example Example Example Example Example Example Example Example Example Example Example Example Example Example Example Example Example Example Exampl"
    Then the task is not created in the application

Create a subtask successfully
    [Tags]  create_subtask
    Given that I have been created a task "Create Layout"
    When I try to create a new subtask "Choose tool" and due date "25/06/2019"
    Then the subtask "Choose tool" is added in my todo list