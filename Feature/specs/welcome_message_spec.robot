*** Settings ***
Resource  ../resources/welcome_message_res.robot

*** Test Case ***
Check if welcome message is correct
    [Tags]  welcome_message
    Given that I have been signed up
    When I sign in using my e-mail and password
    Then I see a message "Hey Tiago, this is your todo list for today:"
