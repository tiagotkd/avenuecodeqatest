from PageObjectLibrary import PageObject
from selenium.webdriver.common.keys import Keys


class TasksPage(PageObject):
    _locators = {
        "welcome_message": "xpath://h1[contains(text(), 'this is your todo list for today')]",
        "new_task": "id=new_task",
        "manage_subtask": "xpath:(//button[contains(text(), 'Manage Subtasks')])[1]",
        "new_subtask": "id=new_sub_task",
        "subtask_duedate": "id=dueDate",
        "add_subtask": "id=add-subtask",
        "remove_task": "xpath://button[contains(text(), 'Remove')]",
        "remove_subtask": "xpath://button[contains(text(), 'Remove SubTask')]"
    }

    def check_welcome_message(self, message):
        custom_element = "xpath://h1[contains(text(), '" + message + "')]"
        self.selib.wait_until_element_is_visible(custom_element)

    def is_task_in_list(self, task_name):
        custom_element = "xpath://a[text()='" + task_name + "']"
        self.selib.wait_until_element_is_visible(custom_element)

    def create_task(self, task_name):
        self._input_task_name(task_name)
        self._enter_task()

    def create_subtask(self, subtask_name, due_date):
        self._click_manage_subtasks()
        self._input_subtask_name(subtask_name)
        self._input_due_date(due_date)
        self._click_add_subtask()

    def remove_task(self):
        self._click_remove_task()

    def remove_subtask(self):
        self._click_remove_subtask()



    def _click_remove_subtask(self):
        self.selib.wait_until_element_is_visible(self.locator.remove_subtask)
        self.selib.click_element(self.locator.remove_subtask)

    def _click_remove_task(self):
        self.selib.wait_until_element_is_visible(self.locator.remove_task)
        self.selib.click_element(self.locator.remove_task)

    def _enter_task(self):
        self.driver.find_element_by_id("new_task").send_keys(Keys.RETURN)

    def _input_task_name(self, task_name):
        self.selib.wait_until_element_is_visible(self.locator.new_task)
        self.selib.input_text(self.locator.new_task, task_name)

    def _click_manage_subtasks(self):
        self.selib.wait_until_element_is_visible(self.locator.manage_subtask)
        self.selib.click_element(self.locator.manage_subtask)

    def _input_subtask_name(self, subtask_name):
        self.selib.wait_until_element_is_visible(self.locator.new_subtask)
        self.selib.input_text(self.locator.new_subtask, subtask_name)

    def _input_due_date(self, due_date):
        self.selib.input_text(self.locator.subtask_duedate, due_date)

    def _click_add_subtask(self):
        self.selib.click_element(self.locator.add_subtask)
