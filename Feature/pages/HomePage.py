from PageObjectLibrary import PageObject

class HomePage(PageObject):

    _locators = {
        "mytasks": "xpath://a[text()='My Tasks']"
    }

    def my_tasks(self):
        self.selib.wait_until_element_is_visible(self.locator.mytasks)
        self.selib.click_element(self.locator.mytasks)