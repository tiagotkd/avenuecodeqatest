from PageObjectLibrary import PageObject


class LoginPage(PageObject):
    PAGE_URL = "https://qa-test.avenuecode.com/users/sign_in"

    _locators = {
        "email": "id=user_email",
        "password": "id=user_password",
        "signin": "name=commit"
    }

    def go_to_login_page(self):
        self.selib.go_to(self.PAGE_URL)
        self._wait_for_page_refresh()

    def login(self, email, password):
        self.selib.wait_until_element_is_visible(self.locator.email)
        self.selib.input_text(self.locator.email, email)
        self.selib.input_text(self.locator.password, password)
        self.selib.click_element(self.locator.signin)
